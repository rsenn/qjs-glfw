# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Default target executed when no arguments are given to make.
default_target: all
.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /opt/cmake-20201216/bin/cmake

# The command to remove a file.
RM = /opt/cmake-20201216/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/roman/Projects/plot-cv

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/roman/Projects/plot-cv

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target install/strip
install/strip: preinstall
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Installing the project stripped..."
	/opt/cmake-20201216/bin/cmake -DCMAKE_INSTALL_DO_STRIP=1 -P cmake_install.cmake
.PHONY : install/strip

# Special rule for the target install/strip
install/strip/fast: preinstall/fast
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Installing the project stripped..."
	/opt/cmake-20201216/bin/cmake -DCMAKE_INSTALL_DO_STRIP=1 -P cmake_install.cmake
.PHONY : install/strip/fast

# Special rule for the target install/local
install/local: preinstall
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Installing only the local directory..."
	/opt/cmake-20201216/bin/cmake -DCMAKE_INSTALL_LOCAL_ONLY=1 -P cmake_install.cmake
.PHONY : install/local

# Special rule for the target install/local
install/local/fast: preinstall/fast
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Installing only the local directory..."
	/opt/cmake-20201216/bin/cmake -DCMAKE_INSTALL_LOCAL_ONLY=1 -P cmake_install.cmake
.PHONY : install/local/fast

# Special rule for the target install
install: preinstall
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Install the project..."
	/opt/cmake-20201216/bin/cmake -P cmake_install.cmake
.PHONY : install

# Special rule for the target install
install/fast: preinstall/fast
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Install the project..."
	/opt/cmake-20201216/bin/cmake -P cmake_install.cmake
.PHONY : install/fast

# Special rule for the target list_install_components
list_install_components:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Available install components are: \"Unspecified\""
.PHONY : list_install_components

# Special rule for the target list_install_components
list_install_components/fast: list_install_components
.PHONY : list_install_components/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/opt/cmake-20201216/bin/cmake --regenerate-during-build -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache
.PHONY : rebuild_cache/fast

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/opt/cmake-20201216/bin/ccmake -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache
.PHONY : edit_cache/fast

# The main all target
all: cmake_check_build_system
	cd /home/roman/Projects/plot-cv && $(CMAKE_COMMAND) -E cmake_progress_start /home/roman/Projects/plot-cv/CMakeFiles /home/roman/Projects/plot-cv/quickjs/qjs-glfw//CMakeFiles/progress.marks
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 quickjs/qjs-glfw/all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/roman/Projects/plot-cv/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 quickjs/qjs-glfw/clean
.PHONY : clean

# The main clean target
clean/fast: clean
.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 quickjs/qjs-glfw/preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 quickjs/qjs-glfw/preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	cd /home/roman/Projects/plot-cv && $(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

# Convenience name for target.
quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/rule:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/rule
.PHONY : quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/rule

# Convenience name for target.
qjs-glfw: quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/rule
.PHONY : qjs-glfw

# fast build rule for target.
qjs-glfw/fast:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build
.PHONY : qjs-glfw/fast

gamma_ramp.o: gamma_ramp.c.o
.PHONY : gamma_ramp.o

# target to build an object file
gamma_ramp.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/gamma_ramp.c.o
.PHONY : gamma_ramp.c.o

gamma_ramp.i: gamma_ramp.c.i
.PHONY : gamma_ramp.i

# target to preprocess a source file
gamma_ramp.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/gamma_ramp.c.i
.PHONY : gamma_ramp.c.i

gamma_ramp.s: gamma_ramp.c.s
.PHONY : gamma_ramp.s

# target to generate assembly for a file
gamma_ramp.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/gamma_ramp.c.s
.PHONY : gamma_ramp.c.s

glfw.o: glfw.c.o
.PHONY : glfw.o

# target to build an object file
glfw.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/glfw.c.o
.PHONY : glfw.c.o

glfw.i: glfw.c.i
.PHONY : glfw.i

# target to preprocess a source file
glfw.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/glfw.c.i
.PHONY : glfw.c.i

glfw.s: glfw.c.s
.PHONY : glfw.s

# target to generate assembly for a file
glfw.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/glfw.c.s
.PHONY : glfw.c.s

monitor.o: monitor.c.o
.PHONY : monitor.o

# target to build an object file
monitor.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/monitor.c.o
.PHONY : monitor.c.o

monitor.i: monitor.c.i
.PHONY : monitor.i

# target to preprocess a source file
monitor.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/monitor.c.i
.PHONY : monitor.c.i

monitor.s: monitor.c.s
.PHONY : monitor.s

# target to generate assembly for a file
monitor.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/monitor.c.s
.PHONY : monitor.c.s

position.o: position.c.o
.PHONY : position.o

# target to build an object file
position.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/position.c.o
.PHONY : position.c.o

position.i: position.c.i
.PHONY : position.i

# target to preprocess a source file
position.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/position.c.i
.PHONY : position.c.i

position.s: position.c.s
.PHONY : position.s

# target to generate assembly for a file
position.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/position.c.s
.PHONY : position.c.s

scale.o: scale.c.o
.PHONY : scale.o

# target to build an object file
scale.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/scale.c.o
.PHONY : scale.c.o

scale.i: scale.c.i
.PHONY : scale.i

# target to preprocess a source file
scale.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/scale.c.i
.PHONY : scale.c.i

scale.s: scale.c.s
.PHONY : scale.s

# target to generate assembly for a file
scale.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/scale.c.s
.PHONY : scale.c.s

size.o: size.c.o
.PHONY : size.o

# target to build an object file
size.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/size.c.o
.PHONY : size.c.o

size.i: size.c.i
.PHONY : size.i

# target to preprocess a source file
size.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/size.c.i
.PHONY : size.c.i

size.s: size.c.s
.PHONY : size.s

# target to generate assembly for a file
size.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/size.c.s
.PHONY : size.c.s

video_mode.o: video_mode.c.o
.PHONY : video_mode.o

# target to build an object file
video_mode.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/video_mode.c.o
.PHONY : video_mode.c.o

video_mode.i: video_mode.c.i
.PHONY : video_mode.i

# target to preprocess a source file
video_mode.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/video_mode.c.i
.PHONY : video_mode.c.i

video_mode.s: video_mode.c.s
.PHONY : video_mode.s

# target to generate assembly for a file
video_mode.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/video_mode.c.s
.PHONY : video_mode.c.s

window.o: window.c.o
.PHONY : window.o

# target to build an object file
window.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/window.c.o
.PHONY : window.c.o

window.i: window.c.i
.PHONY : window.i

# target to preprocess a source file
window.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/window.c.i
.PHONY : window.c.i

window.s: window.c.s
.PHONY : window.s

# target to generate assembly for a file
window.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/window.c.s
.PHONY : window.c.s

workarea.o: workarea.c.o
.PHONY : workarea.o

# target to build an object file
workarea.c.o:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/workarea.c.o
.PHONY : workarea.c.o

workarea.i: workarea.c.i
.PHONY : workarea.i

# target to preprocess a source file
workarea.c.i:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/workarea.c.i
.PHONY : workarea.c.i

workarea.s: workarea.c.s
.PHONY : workarea.s

# target to generate assembly for a file
workarea.c.s:
	cd /home/roman/Projects/plot-cv && $(MAKE) $(MAKESILENT) -f quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/build.make quickjs/qjs-glfw/CMakeFiles/qjs-glfw.dir/workarea.c.s
.PHONY : workarea.c.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... edit_cache"
	@echo "... install"
	@echo "... install/local"
	@echo "... install/strip"
	@echo "... list_install_components"
	@echo "... rebuild_cache"
	@echo "... qjs-glfw"
	@echo "... gamma_ramp.o"
	@echo "... gamma_ramp.i"
	@echo "... gamma_ramp.s"
	@echo "... glfw.o"
	@echo "... glfw.i"
	@echo "... glfw.s"
	@echo "... monitor.o"
	@echo "... monitor.i"
	@echo "... monitor.s"
	@echo "... position.o"
	@echo "... position.i"
	@echo "... position.s"
	@echo "... scale.o"
	@echo "... scale.i"
	@echo "... scale.s"
	@echo "... size.o"
	@echo "... size.i"
	@echo "... size.s"
	@echo "... video_mode.o"
	@echo "... video_mode.i"
	@echo "... video_mode.s"
	@echo "... window.o"
	@echo "... window.i"
	@echo "... window.s"
	@echo "... workarea.o"
	@echo "... workarea.i"
	@echo "... workarea.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	cd /home/roman/Projects/plot-cv && $(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

